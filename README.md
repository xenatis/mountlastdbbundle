# MountLastDbBundle

Symfony bundle to:
- mount last .gz sql dump from remote SMB or FTPS server and execute it. Optional schema drop (doctrine:
schema:
drop --force) and migration execution (d:m:m)
- copy current database to test database. Test DB name is set by curDbName + dbSuffix. Bette to use same db suffix as in doctrine.yaml when@test.
- dump or mount local database
## Install

```
composer require antihero/mount-last-db-bundle
```

## Config

```
mount_last_db:
    srv_type: ftps|smb
    dest_dir: _dump (relative to project root)
    file_filter: lhyo_ocf
    srv_uri: 192.168.0.92
    srv_path: backups
    srv_user: USER
    srv_pwd: '%env(AH_SRV_PWD)%'
    srv_share: mysql_backups (only for smb)
    srv_workgroup: workgroup (only for smb)
    db_host: '%env(DB_HOST)%'
    db_port: '%env(DB_PORT)%'
    db_user: '%env(DB_USERNAME)%'
    db_pwd: '%env(DB_PASSWORD)%'
```

```
php bin/console debug:config mount_last_db
```

## Usage

```
php bin/console ah:mount-last-db
php bin/console ah:mount-db
php bin/console ah:dump-db
php bin/console ah:copy-db-to-test DBSUFFIX
```