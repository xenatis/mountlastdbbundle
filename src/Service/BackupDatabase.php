<?php

namespace AntiHero\MountLastDbBundle\Service;

/**
 * The Backup_Database class.
 */
class BackupDatabase
{
    /**
     * Database connection.
     */
    public \mysqli|bool $conn;

    /**
     * Output backup file.
     */
    public string $backupFile;

    /**
     * Constructor initializes database.
     */
    public function __construct(
        private readonly string $host,
        public string           $dbPort,
        public string           $username,
        public string           $passwd,
        public string           $dbName,
        public string           $charset,
        public string           $backupDir,
        public bool             $gzipBackupFile)
    {
        $this->conn = $this->initializeDatabase();
        $this->backupFile = 'dump-' . $this->dbName . '.sql';
    }

    public function setBackupFilename(string $filename): void
    {
        $this->backupFile = $filename;
    }

    protected function initializeDatabase(): bool|\mysqli
    {
        //        try {
        $conn = mysqli_connect($this->host, $this->username, $this->passwd, $this->dbName, $this->dbPort);
        if (mysqli_connect_errno()) {
            throw new \Exception('ERROR connecting database: ' . mysqli_connect_error());
        }
        if (!mysqli_set_charset($conn, $this->charset)) {
            mysqli_query($conn, 'SET NAMES ' . $this->charset);
        }
        //        } catch (Exception $e) {
        //            print_r($e->getMessage());
        //            die();
        //        }

        return $conn;
    }

    /**
     *  Backup the whole database or just some tables
     *  Use '*' for whole database or 'table1,table2,table3...'.
     *
     * @param array<string>|string $tables
     */
    public function backupTables(array|string $tables = '*'): bool
    {
        try {

            //DELETE PREVIOUS DUMP
            if (is_file($this->backupDir . '/' . $this->backupFile)) {
                unlink($this->backupDir . '/' . $this->backupFile);
            }

            $tables = $this->getTables($tables);

//            $tablesInDB = mysqli_fetch_all(mysqli_query($this->conn, 'SHOW TABLES'));

            $sql = "/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n";

            $row = mysqli_fetch_row(mysqli_query($this->conn, 'SHOW CREATE DATABASE `' . $this->dbName . '`'));

            /*
             * Iterate tables
             */
            foreach ($tables as $table) {
                /*
                 * CREATE TABLE
                 */
                $sql .= 'DROP TABLE IF EXISTS `' . $table . '`;';
                $row = mysqli_fetch_row(mysqli_query($this->conn, 'SHOW CREATE TABLE `' . $table . '`'));
                $sql .= "\n" . $row[1] . ";\n\n";

                /**
                 * INSERT INTO.
                 */
                $row = mysqli_fetch_row(mysqli_query($this->conn, 'SELECT COUNT(*) FROM `' . $table . '`'));
                $numRows = $row[0];

                // Split table in batches in order to not exhaust system memory
                $batchSize = 1000; // Number of rows per batch
                $numBatches = intval($numRows / $batchSize) + 1; // Number of while-loop calls to perform
                for ($b = 1; $b <= $numBatches; ++$b) {
                    $query = 'SELECT * FROM `' . $table . '` LIMIT ' . ($b * $batchSize - $batchSize) . ',' . $batchSize;
                    $result = mysqli_query($this->conn, $query);
                    $numFields = mysqli_num_fields($result);

                    if (mysqli_num_rows($result) > 0) {
                        $sql .= 'INSERT INTO `' . $table . '` VALUES ';
                    }

                    for ($i = 0; $i < $numFields; ++$i) {
                        while ($row = mysqli_fetch_row($result)) {
                            $sql .= '(';
                            for ($j = 0; $j < $numFields; ++$j) {
                                if (isset($row[$j])) {
                                    $row[$j] = addslashes((string)$row[$j]);
                                    $row[$j] = str_replace("\n", '\\n', $row[$j]);
                                    $sql .= '"' . $row[$j] . '"';
                                } else {
                                    $sql .= 'NULL';
                                }

                                if ($j < ($numFields - 1)) {
                                    $sql .= ',';
                                }
                            }

                            $sql .= "),\n";
                        }
                    }
                    if (mysqli_num_rows($result) > 0) {
                        $sql = substr($sql, 0, -2) . ";\n\n";
                    }

                    $this->saveToFile($sql);
                    $sql = '';
                }

                //                if (VERBOSE) Printer::obfPrint("$table OK");
            }
            $sql = '/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;';
            $sql .= "\n\n\n";
            $this->saveToFile($sql);

            if ($this->gzipBackupFile) {
                $this->gzipBackupFile();
            } else {
                //                Printer::obfPrint('Backup file successfully saved to ' . $this->backupDir . '/' . $this->backupFile, 0, 1);
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * Save SQL to file.
     */
    protected function saveToFile(string &$sql): bool
    {
        if (!$sql) {
            return false;
        }

        try {
            if (!file_exists($this->backupDir)) {
                mkdir($this->backupDir, 0777, true);
            }

            file_put_contents($this->backupDir . '/' . $this->backupFile, $sql, FILE_APPEND | LOCK_EX);
        } catch (\Exception $e) {
            print_r($e->getMessage());

            return false;
        }

        return true;
    }

    /*
    * Gzip backup file
    *
    * @param integer $level GZIP compression level (default: 9)
    * @return string New filename (with .gz appended) if success, or false if operation fails
    */
    protected function gzipBackupFile(int $level = 9): bool|string
    {
        if (!$this->gzipBackupFile) {
            return true;
        }

        $source = $this->backupDir . '/' . $this->backupFile;
        $dest = $source . '.gz';

        //        if (VERBOSE) Printer::obfPrint('Gzipping backup file to ' . $dest . '... ', 0, 0);

        $mode = 'wb' . $level;
        if ($fpOut = gzopen($dest, $mode)) {
            if ($fpIn = fopen($source, 'rb')) {
                while (!feof($fpIn)) {
                    gzwrite($fpOut, fread($fpIn, 1024 * 256));
                }
                fclose($fpIn);
            } else {
                return false;
            }
            gzclose($fpOut);
            if (!unlink($source)) {
                return false;
            }
        } else {
            return false;
        }

        //        if (VERBOSE) Printer::obfPrint('OK');
        return $dest;
    }

    /**
     * @param array|string $tables
     * @return array
     */
    public function getTables(array|string $tables): array
    {
        if ($tables == '*') {
            $tables = [];
            //                $result = mysqli_query($this->conn, 'SHOW TABLES');
            $result = mysqli_query($this->conn, 'SHOW FULL TABLES WHERE table_type NOT LIKE \'VIEW\'');
            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }
        return $tables;
    }
}
