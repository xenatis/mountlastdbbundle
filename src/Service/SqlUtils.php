<?php

namespace AntiHero\MountLastDbBundle\Service;

use Doctrine\DBAL\Connection;

class SqlUtils
{
    public static function getSqlCommands(string $destFile, bool $removeCreateAndUse): array
    {
        $file = fopen($destFile, 'r');
        $sqlCommand = '';
        $sqlCommands = [];
        while (($line = fgets($file)) !== false) {

            if ($removeCreateAndUse &&
                (str_contains(trim($line), 'CREATE DATABASE') || str_contains(trim($line), 'USE '))) {
                continue;
            }

            $sqlCommand .= $line;
            if (str_ends_with(str_replace("\n", '', $line), ';')) {
                $sqlCommands[] = $sqlCommand;
                $sqlCommand = '';
            }
        }
        return $sqlCommands;
    }
}