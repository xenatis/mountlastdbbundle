<?php

namespace AntiHero\MountLastDbBundle\Service;

class Archive
{


    public function __construct(private readonly string $sourceFile)
    {
    }

    public function extract(string $destFile):void
    {
        $buffer_size = 4096; // read 4kb at a time
        $file = gzopen($this->sourceFile, 'rb');
        $out_file = fopen($destFile, 'wb');
        while (!gzeof($file)) {
            // Read buffer-size bytes
            // Both fwrite and gzread and binary-safe
            fwrite($out_file, gzread($file, $buffer_size));
        }

        fclose($out_file);
        gzclose($file);

    }
}