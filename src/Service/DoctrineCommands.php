<?php

namespace AntiHero\MountLastDbBundle\Service;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class DoctrineCommands
{

    public function dropTables(OutputInterface $output, KernelInterface $kernel): void
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $cmd = new ArrayInput([
            'command' => 'doctrine:schema:drop',
            '--full-database' => true,
            '--force' => true,
        ]);

        $application->run($cmd, $output);
    }

    public function execMigrations(OutputInterface $output, KernelInterface $kernel): void
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $cmd = new ArrayInput([
            'command' => 'd:m:m',
        ]);

        $application->run($cmd, $output);
    }
}