<?php

namespace AntiHero\MountLastDbBundle\Service;

use FTP\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Filesystem\Filesystem;

class FtpConnection
{
    public ?string $errorMessage = null;
    private ?Connection $ftpConn = null;

    public function __construct(private readonly array $params)
    {
    }

    public function login(): void
    {
        $this->ftpConn = ftp_ssl_connect($this->params['srv_uri']);
        if (!$this->ftpConn) {
            $this->errorMessage = 'Could not connect to ' . $this->params['srv_uri'];
            throw new \Exception($this->errorMessage);
        }

        $login = ftp_login($this->ftpConn, $this->params['srv_user'], $this->params['srv_pwd']);
        if (!$login) {
            $this->errorMessage = 'Could not login to ' . $this->params['srv_uri'];
            throw new \Exception($this->errorMessage);
        }

        ftp_pasv($this->ftpConn, true);
    }

    public function downloadArchive(bool $forceDownload):string
    {
        if (!ftp_chdir($this->ftpConn, $this->params['srv_path'])) {
            $this->errorMessage = 'Couldn’t change to directory ' . $this->params['srv_path'];
            throw new \Exception($this->errorMessage);
        }

        $files = ftp_nlist($this->ftpConn, '.');
        if (!is_array($files)) {
            $this->errorMessage = 'Couldn’t get file list';
            throw new \Exception($this->errorMessage);
        }

        $distFiles = [];
        foreach ($files as $file) {
            if (str_contains($file, $this->params['file_filter'])) {
                $a = ftp_mdtm($this->ftpConn, $file);
                $distFiles[] = ['file' => $file, 'timestamp' => $a];
            }
        }

        usort($distFiles, function (array $a, array $b) {
            return $b['timestamp'] <=> $a['timestamp'];
        });
        $distFile = $distFiles[0]['file'];
        if (count($distFiles) === 0) {
            $this->errorMessage = 'No file found in ' . $this->params['srv_path'] . ' with filter ' . $this->params['file_filter'];
            throw new \Exception($this->errorMessage);
        }

        // CREATE DEST DIR
        $destDir = $this->params['dest_dir'];
        $destDir = str_ends_with($destDir, '/') ? substr($destDir, 0, -1) : $destDir;
        $destArchiveFile = $destDir . DIRECTORY_SEPARATOR . $distFile;
        $fs = new Filesystem();
        $fs->mkdir($destDir);

        if ($forceDownload || !file_exists($destArchiveFile)) {
            $getFile = ftp_get($this->ftpConn, $destArchiveFile, $distFile);
            if (!$getFile) {
                $this->errorMessage = 'Error downloading file. ' . $destArchiveFile;
                throw new \Exception($this->errorMessage);
            }
        }
        return $destArchiveFile;
    }

    public function close(): void{
        ftp_close($this->ftpConn);
    }
}