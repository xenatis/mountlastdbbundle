<?php

namespace AntiHero\MountLastDbBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class MountLastDbExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new PhpFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.php');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('ah.bundle.mount_last_db');
        $definition->setArgument(2, $config);

        $definition = $container->getDefinition('ah.bundle.dump_db');
        $definition->setArgument(0, $config);

        $definition = $container->getDefinition('ah.bundle.mount_db');
        $definition->setArgument(2, $config);

        $definition = $container->getDefinition('ah.bundle.copy_db_to_test');
        $definition->setArgument(0, $config);
    }
}
