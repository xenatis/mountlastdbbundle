<?php

namespace AntiHero\MountLastDbBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('mount_last_db');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->enumNode('srv_type')->values(['smb', 'ftps'])->defaultValue('ftps')->end()
            ->variableNode('dest_dir')->defaultValue('_dump')->end()
            ->variableNode('file_filter')->defaultValue('lhyo_ocf')->end()
            ->variableNode('srv_uri')->defaultValue('192.168.0.92')->end()
            ->variableNode('srv_path')->defaultValue('backups')->end()
            ->variableNode('srv_user')->defaultValue('USER')->end()
            ->variableNode('srv_pwd')->defaultValue('%env(AH_SRV_PWD)%')->end()
            ->variableNode('srv_share')->defaultValue('mysql_backups')->end()
            ->variableNode('srv_workgroup')->defaultValue('workgroup')->end()
            ->variableNode('db_host')->defaultValue('%env(DB_HOST)%')->end()
            ->variableNode('db_port')->defaultValue('%env(DB_PORT)%')->end()
            ->variableNode('db_name')->defaultValue('%env(DB_NAME)%')->end()
            ->variableNode('db_user')->defaultValue('%env(DB_USERNAME)%')->end()
            ->variableNode('db_pwd')->defaultValue('%env(DB_PASSWORD)%')->end()
            ->end();

        return $treeBuilder;
    }
}
