<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use AntiHero\MountLastDbBundle\Command\CopyDbToTestCommand;
use AntiHero\MountLastDbBundle\Command\DumpDbCommand;
use AntiHero\MountLastDbBundle\Command\MountDbCommand;
use AntiHero\MountLastDbBundle\Command\MountLastDbCommand;

return static function (ContainerConfigurator $container) {
    $services = $container->services()
        ->defaults()->private();

    $services
        ->set('ah.bundle.mount_last_db', MountLastDbCommand::class)->public()

        ->arg(0, service('doctrine.orm.entity_manager'))
        ->arg(1, service('kernel'))
        ->arg(2, [])
        ->tag('console.command')
        ->alias('ah.mount_last_db', 'ah.bundle.mount_last_db')

        ->set('ah.bundle.copy_db_to_test', CopyDbToTestCommand::class)->public()
        ->tag('console.command')
        ->alias('ah.copy-db-to-test', 'ah.bundle.copy_db_to_test')

        ->set('ah.bundle.dump_db', DumpDbCommand::class)->public()
        ->tag('console.command')
        ->alias('ah.dump_db', 'ah.bundle.dump_db')

        ->set('ah.bundle.mount_db', MountDbCommand::class)->public()
        ->arg(0, service('doctrine.orm.entity_manager'))
        ->arg(1, service('kernel'))
        ->tag('console.command')
        ->alias('ah.mount_db', 'ah.bundle.mount_db')

        ->set('ah.bundle.copy_db_to_test', CopyDbToTestCommand::class)->public()
        ->tag('console.command')
        ->alias('ah.copy-db-to-test', 'ah.bundle.copy_db_to_test')
    ;
};
