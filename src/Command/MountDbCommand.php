<?php

namespace AntiHero\MountLastDbBundle\Command;

use AntiHero\MountLastDbBundle\Service\DoctrineCommands;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'ah:mount-db',
    description: 'Mount DB dumped with dump-db command',
)]
class MountDbCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly KernelInterface $kernel, private readonly array $params)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        //        $this
        //            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        //            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        //        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $sqlFile = $this->params['dest_dir'].'/dump-'.$this->params['db_name'].'.sql';
        if (!file_exists($sqlFile)) {
            $io->error('File '.$sqlFile.' not found.');

            return Command::FAILURE;
        }
        $sql = file_get_contents($sqlFile);

        // DROP TABLES
        $res = $io->ask('Drop tables before import? [y|n]', 'y');
        if (strtolower($res) === 'y') {
            (new DoctrineCommands)->dropTables($output,$this->kernel);
        }

        // RUN SCRIPT
        $io->text('Mounting sql...');
        $sqlCommands = explode(';'.PHP_EOL, $sql);
        $progressBar = new ProgressBar($output, count($sqlCommands));
        $progressBar->start();
        foreach ($sqlCommands as $command) {
            $progressBar->advance();
            try {
                $this->entityManager->getConnection()->executeStatement($command);
            } catch (\Exception $ex) {
                $io->warning($ex->getMessage().PHP_EOL.'SQL:"'.$command.'"');
            }
        }
        $progressBar->finish();
        $io->success('DB mounted.');

        return Command::SUCCESS;
    }
}
