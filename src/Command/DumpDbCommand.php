<?php

namespace AntiHero\MountLastDbBundle\Command;

use AntiHero\MountLastDbBundle\Service\BackupDatabase;
use Druidfi\Mysqldump\Mysqldump;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'ah:dump-db',
    description: 'Dump DB to .sql file.',
)]
class DumpDbCommand extends Command
{
    public function __construct(private readonly array $params)
    {
        parent::__construct();
    }


    protected function configure(): void
    {
        //        $this
        //            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        //            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        //        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return $this->dumpDatabase($input, $output);

    }

    private function checkEnvVariables():void
    {
        if ($_ENV['DB_HOST'] == null) {
            throw new \Exception('DB_HOST env variable is required.');
        }

        if ($_ENV['DB_EXPOSED_PORT'] == null) {
            throw new \Exception('DB_PORT env variable is required.');
        }

        if ($_ENV['DB_USERNAME'] == null) {
            throw new \Exception('DB_USERNAME env variable is required.');
        }

        if ($_ENV['DB_PASSWORD'] == null) {
            throw new \Exception('DB_PASSWORD env variable is required.');
        }

        if ($_ENV['DB_NAME'] == null) {
            throw new \Exception('DB_NAME env variable is required.');
        }
    }
    public function dumpDatabase(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

//        try {
//            $this->checkEnvVariables();
//        } catch (\Exception $e) {
//            $io->error($e->getMessage());
//            return Command::FAILURE;
//        }

        $fs = new Filesystem();
        $fs->mkdir($this->params['dest_dir']);

        $backupDatabase = new BackupDatabase(
            $this->params['db_host'],
            $this->params['db_port'],
            $this->params['db_user'],
            $this->params['db_pwd'],
            $this->params['db_name'],
            'utf8',
            //            '_dumps',
            $this->params['dest_dir'],
            false);

        $result = $backupDatabase->backupTables('*');
        if ($result) {
            $io->success('Backup success');

            return Command::SUCCESS;
        } else {
            $io->error('Backup failed');

            return Command::FAILURE;
        }
    }

//    private function dumpDatabase(InputInterface $input, OutputInterface $output): int
//    {
//        $io = new SymfonyStyle($input, $output);
//
//        try {
//            $this->checkEnvVariables();
//        } catch (\Exception $e) {
//            $io->error($e->getMessage());
//            return Command::FAILURE;
//        }
//
//        try {
//            $connectionString = 'mysql:dbname='.$_ENV['DB_NAME'].';host='.$_ENV['DB_HOST'].';port='.$_ENV['DB_EXPOSED_PORT'];
//            $dump = new Mysqldump($connectionString, $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD']);
//            $dump->start($this->params['dest_dir'].'/'.'dump-'.$_ENV['DB_NAME'].'.sql');
//        } catch (\Exception $e) {
//            echo 'mysqldump-php error: ' . $e->getMessage();
//            return Command::FAILURE;
//        }
//
//        return Command::SUCCESS;
//    }
}
