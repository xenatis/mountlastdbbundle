<?php

namespace AntiHero\MountLastDbBundle\Command;

use AntiHero\MountLastDbBundle\Service\BackupDatabase;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'ah:copy-db-to-test',
    description: 'Copy configured DB to test DB. Use -dbSuffix to specify the suffix of the test DB. Default is _test.',
)]
class CopyDbToTestCommand extends Command
{
    public function __construct(private readonly array $params)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('dbSuffix', InputArgument::OPTIONAL, 'db name suffix. Use what is configured in doctrine.yaml at when@test', '_test')
        //            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $backupDir = '_dumps';
        $charset = 'utf8';
        $dbSuffix = $input->getArgument('dbSuffix');

        $backupDatabase = new BackupDatabase(
            $this->params['db_host'],
            $this->params['db_port'],
            $this->params['db_user'],
            $this->params['db_pwd'],
            $this->params['db_name'],
            $charset,
            $backupDir,
            false);
        $filename = 'temp.sql';

        // CREATE DEST DIR
        $destDir = str_ends_with($backupDir, '/') ? substr($backupDir, 0, -1) : $backupDir;
        $fs = new Filesystem();
        $fs->mkdir($destDir);

        $io->text('Dumping DB...');
        $backupDatabase->setBackupFilename($filename);
        $result = $backupDatabase->backupTables('*');

        if (!$result) {
            $io->error('Backup failed');
            return Command::FAILURE;
        }

        // RUN SCRIPT
        $io->text('Mounting sql...');

        //        $conn = mysqli_connect($_ENV['DB_HOST'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD'], $_ENV['DB_NAME'], $_ENV['DB_PORT']);
        $conn = mysqli_connect($this->params['db_host'], $this->params['db_user'], $this->params['db_pwd'], $this->params['db_name'], $this->params['db_port']);
        if (mysqli_connect_errno()) {
            $io->error('ERROR connecting database: '.mysqli_connect_error());

            return Command::FAILURE;
        }
        if (!mysqli_set_charset($conn, $charset)) {
            $conn->query($conn, 'SET NAMES '.$charset);
        }

        $sql = 'CREATE DATABASE IF NOT EXISTS `'.$this->params['db_name'].$dbSuffix.'`';
        $conn->query($sql);

        $sql = 'GRANT ALL PRIVILEGES ON `'.$this->params['db_name'].$dbSuffix."`.* TO '".$this->params['db_user']."'@'%'";// identified by '".$this->params['db_pwd']."'";
        $conn->query($sql);

        $sql = 'use `'.$this->params['db_name'].$dbSuffix.'`';
        $conn->query($sql);

        $sql = file_get_contents('./'.$backupDir.'/'.$filename);
        $sqlCommands = explode(';'.PHP_EOL, $sql);
        $progressBar = new ProgressBar($output, count($sqlCommands));
        $progressBar->start();
        foreach ($sqlCommands as $command) {
            $progressBar->advance();
            try {
                // $this->entityManager->getConnection()->executeStatement($command);
                $conn->query($command);
            } catch (\Exception $ex) {
                $io->warning($ex->getMessage().PHP_EOL.'SQL:"'.$command.'"');
            }
        }
        $progressBar->finish();
        $io->success('Test DB mounted.');

        return Command::SUCCESS;
    }
}
