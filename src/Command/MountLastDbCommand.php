<?php

namespace AntiHero\MountLastDbBundle\Command;

use AntiHero\MountLastDbBundle\Service\Archive;
use AntiHero\MountLastDbBundle\Service\DoctrineCommands;
use AntiHero\MountLastDbBundle\Service\FtpConnection;
use AntiHero\MountLastDbBundle\Service\SqlUtils;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Icewind\SMB\BasicAuth;
use Icewind\SMB\IFileInfo;
use Icewind\SMB\ServerFactory;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'ah:mount-last-db',
    description: 'Mount last .gz sql dump from remote SMB or FTPS server and execute it.'
)]
class MountLastDbCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface $entityManager, private readonly KernelInterface $kernel, private readonly array $params)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // DROP TABLES
        $doDropTables = false;
        $res = $io->ask('Drop tables before import? No drop if download fails. [y|n]', 'y');
        if (strtolower($res) === 'y') {
            $doDropTables = true;
        }

        if ($this->params['srv_type'] == 'smb') {
            // GET FILES
            $io->text('Getting file list...');
            $serverFactory = new ServerFactory();
            $auth = new BasicAuth($this->params['srv_user'], $this->params['srv_workgroup'], $this->params['srv_pwd']);
            $server = $serverFactory->createServer($this->params['srv_uri'], $auth);

            $share = $server->getShare($this->params['srv_share']);
            $files = $share->dir($this->params['srv_path']);

            $dumpFiles = [];
            foreach ($files as $file) {
                if (str_contains($file->getName(), $this->params['file_filter'])) {
                    $dumpFiles[] = $file;
                }
            }

            usort($dumpFiles, function (IFileInfo $a, IFileInfo $b) {
                if ($a->getMTime() === $b->getMTime()) {
                    return 0;
                }

                return $a->getMTime() > $b->getMTime() ? -1 : 1;
            });

            $dumpfile = $dumpFiles[0];
            $io->text('File found: ' . $dumpfile->getName());

            // CREATE DEST DIR
            $destDir = $this->params['dest_dir'];
            $destDir = str_ends_with($destDir, '/') ? substr($destDir, 0, -1) : $destDir;
            $archiveFile = $destDir . DIRECTORY_SEPARATOR . $dumpfile->getName();
            $fs = new Filesystem();
            $fs->mkdir($destDir);

            try {
                $fileHandle = $share->read($dumpfile->getPath());
                $content = '';
                while (!feof($fileHandle)) {
                    $content .= fread($fileHandle, 8192);
                }
                file_put_contents($archiveFile, $content);
            } catch (\Exception $e) {
                $io->error($e->getMessage());
            }
        } else {

            $ftp = new FtpConnection($this->params);
            try {
                $ftp->login();
                $archiveFile = $ftp->downloadArchive(false);
            } catch (\Exception $e) {
                $io->error($e->getMessage());
                return Command::FAILURE;
            }
        }

        if ($doDropTables) {
            (new DoctrineCommands)->dropTables($output,$this->kernel);
        }

        // UNZIP FILE
        $io->text('Unzipping ' . $archiveFile . '...');
        $archive = new Archive($archiveFile);
        $destSqlFile = str_replace('.gz', '', $archiveFile);

        $archive->extract($destSqlFile);

        // CLEAN SQL
        $io->text('Removing CREATE and USE sql commands...');

        $sqlCommands = SqlUtils::getSqlCommands($destSqlFile, true);

        // TEST CONNECTION TO DB
        $io->text('Testing connection to DB...');
        try {
            $this->entityManager->getConnection()->executeQuery('SHOW DATABASES')->fetchAllAssociative();
        } catch (\Exception $ex) {
            $io->error($ex->getMessage());

            return Command::FAILURE;
        }

        // RUN SCRIPT
        $io->text('Mounting database...');
        // $sqlCommands = explode(';' . PHP_EOL, $sql);
        $progressBar = new ProgressBar($output, count($sqlCommands));
        $progressBar->start();

        $conn = $this->entityManager->getConnection();

        foreach ($sqlCommands as $command) {
            $progressBar->advance();
            try {
                $conn->executeStatement($command);
            } catch (Exception $ex) {
                $io->warning($ex->getMessage() . PHP_EOL . 'SQL:"' . $command . '"');

                return Command::FAILURE;
            }
        }
        $progressBar->finish();

        // MIGRATIONS
        $res = $io->ask('Exec migrations? [y|n]', 'y');
        if (strtolower($res) === 'y') {
            $io->text('Executing migrations...');
            (new DoctrineCommands)->execMigrations($output,$this->kernel);
        }

        return Command::SUCCESS;
    }

}
