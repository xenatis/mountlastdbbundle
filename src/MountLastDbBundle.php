<?php

namespace AntiHero\MountLastDbBundle;

use AntiHero\MountLastDbBundle\DependencyInjection\MountLastDbExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MountLastDbBundle extends Bundle
{
    public function getContainerExtension(): ?\Symfony\Component\DependencyInjection\Extension\ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new MountLastDbExtension();
        }

        return $this->extension;
    }
}
